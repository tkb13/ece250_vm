#!/bin/bash

set -x # verbose echo all commands
set -e # abort with failure if any command fails


######################################################################
if ! sudo grep -q 'US/Eastern' /etc/timezone; then
    echo 'US/Eastern' | sudo tee /etc/timezone
    sudo dpkg-reconfigure --frontend noninteractive tzdata
fi

######################################################################
# basics:
sudo apt-get -qq update
sudo apt-get -q --yes autoremove
sudo apt-get -q --yes -o Dpkg::Options::="--force-confold" dist-upgrade  
# ^ the --force-confold thing will make "What do you want to do about modified configuration file" questions default to N (keep your currently-installed version)
sudo apt-get -q --yes install wget nano vim man screen curl

######################################################################
# base gui stuff
sudo apt-get -qq --yes install --no-install-recommends xfce4 vnc4server xfce4-terminal xfonts-base xubuntu-icon-theme gksu gedit xterm

######################################################################
# java:
sudo apt-get -q --yes install default-jre default-jdk

######################################################################
# dev stuff:
sudo apt-get -q --yes install valgrind dos2unix build-essential gcc gdb manpages-dev cmake git

######################################################################
# spim:
sudo apt-get -q --yes install spim

######################################################################
# Enable user CIFS homedirs:
git clone https://gitlab.oit.duke.edu/devil-ops/cifs-homedirs
pushd cifs-homedirs
./install.sh
popd

#####################################################################
# Add pam race condition fixer (uses a script installed by cifs-homedirs above)
echo '' >> /etc/pam.d/sshd
echo '# DUKE: Wait for home directory before proceeding (hack to bypass a pam race condition) -- created by Drew Stinnett <drew.stinnett@duke.edu>' >> /etc/pam.d/sshd
echo 'session     optional     pam_exec.so debug seteuid /usr/local/bin/wait_for_homedir.sh' >> /etc/pam.d/sshd


######################################################################
# Add home directory switching tool to path
sudo cp -v homedir /usr/local/bin
sudo chmod +x /usr/local/bin/homedir

