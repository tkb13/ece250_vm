## Synopsis

This code is used to deploy an Application Stack on an Ubuntu 16.04 VM to set up a VM for use in the
'ECE/COMPSCI 250 - Computer Architecture' course. It should be run from the host on which you want it deployed, using an ansible playbook.

Adapted from the CS216 package by Liz Wendland


## Usage
As root:
```
./install.sh
```

