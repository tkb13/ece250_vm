#!/bin/bash

set -x

pwd >> /var/log/appstack_setup.txt

./vm_install.sh 2>&1 | tee /tmp/rapid_image_status.txt | tee -a /var/log/appstack_setup.txt

if [[ ${PIPESTATUS[0]} == 0 ]]; then
  echo "Installer Succeeded"
  echo "success" > /tmp/rapid_image_complete
else
  echo "Installer failed"
  echo "fail" > /tmp/rapid_image_complete
fi
